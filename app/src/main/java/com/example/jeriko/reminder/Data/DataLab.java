package com.example.jeriko.reminder.Data;

import com.example.jeriko.reminder.Tasks.SimpleRecord;

import java.util.ArrayList;

public class DataLab {
    private static DataItemTasks itemTasks;
    private static DataRegularTasks regularTasks;
    private static DataTimeTasks timeTasks;
    private static DataNotes notes;
    private ArrayList<SimpleRecord> dataItems;
    private static DataLab dataLab;

    private DataLab(){
        itemTasks = DataItemTasks.get();
        regularTasks = DataRegularTasks.get();
        timeTasks = DataTimeTasks.get();
        notes = DataNotes.get();
    }
    public ArrayList<SimpleRecord> makeGetArray(){
         dataItems = new ArrayList<>();

        System.out.println("DATATIME " + DataTimeTasks.get().getListTimeTasks().size() + "\n" +
                "DATAREGULAR " + DataRegularTasks.get().getListRegularTasks().size() + "\n" + "DATAITEM " + DataItemTasks.get().getListItemTasks().size() + "\n" +
                "DATANOTE " + DataNotes.get().getListNote().size());

        for(SimpleRecord it : itemTasks.getListItemTasks()){
            dataItems.add(it);
        }
        for(SimpleRecord rt : regularTasks.getListRegularTasks()){
            dataItems.add(rt);
        }
        for(SimpleRecord tt : timeTasks.getListTimeTasks()){
            dataItems.add(tt);
        }
        for(SimpleRecord n : notes.getListNote()){
            dataItems.add(n);
        }

        sortList();

        return dataItems;
    }
    public static DataLab get(){
        if(dataLab==null)
            dataLab = new DataLab();
        return dataLab;
    }
    private void sortList(){
        for(int i=0;i<dataItems.size();i++){
            for(int j=i+1;j<dataItems.size();j++){
                if(dataItems.get(i).getDateCreate().before(dataItems.get(j).getDateCreate()))
                {
                    SimpleRecord record = dataItems.get(i);
                    dataItems.set(i,dataItems.get(j));
                    dataItems.set(j,record);
                }
            }
        }
    }
    public static void clearAll(){
        timeTasks.getListTimeTasks().clear();
        itemTasks.getListItemTasks().clear();
        regularTasks.getListRegularTasks().clear();
        notes.getListNote().clear();
        DataGroups.get().getGroupRecordList().clear();
    }

}
