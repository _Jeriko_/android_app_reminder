package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.Note;

import java.util.ArrayList;
import java.util.Date;

public class NoteActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);
    }
    public void addNewNote(View view){
            EditText text = findViewById(R.id.notes);
            EditText group = findViewById(R.id.group_note);

            ArrayList<String> list = new ArrayList<>();
            String[]arr = text.getText().toString().split("\n");

            for(String s : arr)
                list.add(s);

            Note note = new Note(new Date(),new Date(),list,group.getText().toString());
            DataNotes.get().add(note);

            DataGroups.get().add(new GroupRecord(note));

        JSON.get().saveAndWrite(this);
            finish();
        }
        public void cancelNote(View view){
            finish();
        }
    }