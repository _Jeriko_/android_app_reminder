package com.example.jeriko.reminder.Data;

import android.widget.Toast;

import com.example.jeriko.reminder.Activities.MainActivity;
import com.example.jeriko.reminder.Tasks.GroupRecord;

import java.util.ArrayList;

public class DataGroups {
    private ArrayList<GroupRecord> list;
    private static DataGroups dataGroups;

    private DataGroups(){
        list = new ArrayList<>();
    }
    public static DataGroups get(){
        if(dataGroups==null)
            dataGroups = new DataGroups();
        return dataGroups;
    }
    public void add(GroupRecord group){
        if(!containsSameName(group))
            list.add(group);
    }
    public ArrayList<GroupRecord> getGroupRecordList() {
        return list;
    }
    private boolean containsSameName(GroupRecord group){
        for (GroupRecord g : list){
            if(g.getName().compareTo(group.getName())==0) {
                g.getList().add(group.getList().get(0));
                return true;
            }
        }
        return false;
    }
}
