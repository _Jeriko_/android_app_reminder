package com.example.jeriko.reminder.Tasks;

import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;
import java.util.Date;

public class TimeTask extends Task {
    private String title;
    private Date dateCreate;
    private Date dateChange;
    private ArrayList<String> tasksList;
    private int type;
    private String group;

    public TimeTask(String title,Date dateCreate,Date dateChange,ArrayList<String> tasksList,String group){
        super(title,dateCreate,dateChange,tasksList,group,TIME_TASK);
    }
}
