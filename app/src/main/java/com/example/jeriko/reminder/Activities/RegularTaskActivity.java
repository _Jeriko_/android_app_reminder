package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.RegularTask;

import java.util.ArrayList;
import java.util.Date;

public class RegularTaskActivity extends Activity {
    @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.add_regular_task);
    }
    public void addNewRegularTask(View view){
        EditText title = findViewById(R.id.title_regular_task);
        EditText text = findViewById(R.id.tasks_regular);
        EditText group = findViewById(R.id.group_regular_task);

        ArrayList<String> list = new ArrayList<>();
        String[]arr = text.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        RegularTask newTask = new RegularTask(title.getText().toString(),new Date(),new Date(),list,group.getText().toString());
        DataRegularTasks.get().add(newTask);

        DataGroups.get().add(new GroupRecord(newTask));

        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelRegularTask(View view){
        finish();
    }
}
