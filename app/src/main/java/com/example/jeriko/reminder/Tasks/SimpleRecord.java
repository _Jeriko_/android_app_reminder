package com.example.jeriko.reminder.Tasks;

import java.util.ArrayList;
import java.util.Date;

public class SimpleRecord {
    private Date dateCreate;
    private Date dateChange;
    private ArrayList<String> list;
    private String group;
    private int type;

    public final static int NOTE=4;

    public SimpleRecord(Date dateCreate,Date dateChange,ArrayList<String> list,String group,int type){
        this.dateCreate = dateCreate;
        this.dateChange = dateChange;
        this.list = list;
        this.group = group;
        this.type = type;
    }
    public String listToString(){
        String str = new String();

        for(int i=0;i<list.size()-1;i++)
            str+=list.get(i) + "\n";

        str+=list.get(list.size()-1);

        return str;
    }

    public Date getDateCreate() {
        return dateCreate;
    }
    public Date getDateChange() {
        return dateChange;
    }
    public ArrayList<String> getList() {
        return list;
    }
    public String getGroup() {
        return group;
    }
    public int getType(){
        return type;
    }
}
