package com.example.jeriko.reminder.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;

import java.util.ArrayList;

public class GroupListFragment extends Fragment {
    private RecyclerView recyclerView;
    private GroupAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_list,container,false);

        recyclerView = view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);

        updateUI();

        return view;
    }
    public void updateUI(){
        ArrayList<GroupRecord> list = DataGroups.get().getGroupRecordList();
        adapter = new GroupAdapter(list);

        recyclerView.setAdapter(adapter);
    }

    private class GroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView nameGroup;
        private TextView dateCreate;
        private GroupRecord record;

        public GroupHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.group_record,parent,false));

            nameGroup = itemView.findViewById(R.id.group_name);
            dateCreate = itemView.findViewById(R.id.group_date);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }
        public void bind(GroupRecord record){
            this.record = record;

            nameGroup.setText(record.getName());
            dateCreate.setText(record.getDateCreate().toString());
        }

        @Override
        public void onClick(View v) {
            FragmentManager fm = getFragmentManager();

            Fragment fragment = fm.findFragmentById(R.id.container);

            if(fragment!=null)
                fm.beginTransaction().remove(fragment).commit();

            Fragment fragmentNew = new RecordGroupListFragment();

            Bundle bundle = new Bundle();
            bundle.putString("Name",record.getName());

            fragmentNew.setArguments(bundle);
            fm.beginTransaction()
                    .add(R.id.container,fragmentNew)
                    .commit();
        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(getActivity(),"LONG CLICK",Toast.LENGTH_LONG).show();
            return true;
        }
    }
    private class GroupAdapter extends RecyclerView.Adapter<GroupHolder>{
        private ArrayList<GroupRecord> list;

        public GroupAdapter(ArrayList<GroupRecord> list){
            this.list = list;
        }
        @NonNull
        @Override
        public GroupHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new GroupHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull GroupHolder groupHolder, int i) {
            groupHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
