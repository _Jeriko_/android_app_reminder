package com.example.jeriko.reminder.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

public class TaskDeleteDialogFragment extends DialogFragment {
    private int clickIndex;
    private int type;
    private SimpleRecord deletedTask;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        clickIndex = getArguments().getInt("Index");
        type = getArguments().getInt("Type");
        deletedTask = DataLab.get().makeGetArray().get(clickIndex);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete task?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (type) {
                                    case Task.TIME_TASK:
                                        DataTimeTasks.get().getListTimeTasks().remove(deletedTask);
                                        JSON.get().saveAndWrite(getContext());
                                        updateFragment();
                                        break;
                                    case Task.REGULAR_TASK:
                                        DataRegularTasks.get().getListRegularTasks().remove(deletedTask);
                                        JSON.get().saveAndWrite(getContext());
                                        updateFragment();
                                        break;
                                    case Task.ITEM_TASK:
                                        DataItemTasks.get().getListItemTasks().remove(deletedTask);
                                        JSON.get().saveAndWrite(getContext());
                                        updateFragment();
                                        break;
                                    case Task.NOTE:
                                        DataNotes.get().getListNote().remove(deletedTask);
                                        JSON.get().saveAndWrite(getContext());
                                        updateFragment();
                                        break;
                                }
                            }
                        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("BUTTON NO CLICK\nIndex - " + clickIndex);
                    }
                });
        return builder.create();
    }
    private void updateFragment(){
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.container);

        if(fragment!=null) {
            fm.beginTransaction().remove(fragment).commit();
        }

        Fragment fragmentNew = new TaskListFragment();
        fm.beginTransaction()
                .add(R.id.container,fragmentNew)
                .commit();
    }

}