package com.example.jeriko.reminder.JSON;

import android.content.Context;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.ItemTask;
import com.example.jeriko.reminder.Tasks.Note;
import com.example.jeriko.reminder.Tasks.RegularTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;
import com.example.jeriko.reminder.Tasks.TimeTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class JSON {
    private final String fileName = "saveFile.json";
    private static JSON json;

    private JSON(){
    }
    public static JSON get(){
        if(json==null)
            json = new JSON();
        return json;
    }

    public void saveAndWrite(Context ctx){
        List<SimpleRecord> list = DataLab.get().makeGetArray();

        JSONArray jsonArray = new JSONArray();

        try {
            for (SimpleRecord r : list) {
                JSONObject jsonObject = new JSONObject();

                if(r instanceof Task) {
                    jsonObject.put("Title", ((Task)r).getTitle());
                }
                jsonObject.put("Create Date", r.getDateCreate().getTime());
                jsonObject.put("Change Date", r.getDateChange().getTime());
                jsonObject.put("Tasks", r.listToString());
                jsonObject.put("Type", r.getType());
                jsonObject.put("Group", r.getGroup());

                jsonArray.put(jsonObject);

            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }

        FileOutputStream fos;

        try {
            fos = ctx.openFileOutput(fileName,Context.MODE_PRIVATE);

            fos.write(jsonArray.toString().getBytes());
            fos.close();
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        //DataLab.clearAll();
    }
    public void readJson(Context ctx){
        FileInputStream fis = null;
        String json=null;
        try{
            fis = ctx.openFileInput(fileName);

            if(fis==null)
                return;

            byte[]arr = new byte[fis.available()];
            fis.read(arr);

            json = new String(arr);
            System.out.println("READ: " + json);
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }


        try {
            JSONArray jsonArray = new JSONArray(json);
            for (int i=0;i<jsonArray.length();i++){
                JSONObject obj = (JSONObject)jsonArray.get(i);

                switch ((int)obj.get("Type")){
                    case Task.TIME_TASK:
                        TimeTask tt = new TimeTask((String)obj.get("Title"), new Date((long)obj.get("Create Date")),new Date((long)obj.get("Change Date")),getList((String)obj.get("Tasks")),(String)obj.get("Group"));
                        DataTimeTasks.get().add(tt);
                        DataGroups.get().add(new GroupRecord(tt));
                        break;
                    case Task.ITEM_TASK:
                        ItemTask it = new ItemTask((String)obj.get("Title"), new Date((long)obj.get("Create Date")),new Date((long)obj.get("Change Date")),getList((String)obj.get("Tasks")),(String)obj.get("Group"));
                        DataItemTasks.get().add(it);
                        DataGroups.get().add(new GroupRecord(it));
                        break;
                    case Task.REGULAR_TASK:
                        RegularTask rt =new RegularTask((String)obj.get("Title"), new Date((long)obj.get("Create Date")),new Date((long)obj.get("Change Date")),getList((String)obj.get("Tasks")),(String)obj.get("Group"));
                        DataRegularTasks.get().add(rt);
                        DataGroups.get().add(new GroupRecord(rt));
                        break;
                    case Task.NOTE:
                        Note n = new Note(new Date((long)obj.get("Create Date")),new Date((long)obj.get("Change Date")),getList((String)obj.get("Tasks")),(String)obj.get("Group"));
                        DataNotes.get().add(n);
                        DataGroups.get().add(new GroupRecord(n));
                        break;
                }
            }
        }
        catch(JSONException ex){
            ex.printStackTrace();
        }
    }
    private ArrayList<String> getList(String str){

        ArrayList<String> list = new ArrayList<>();

        String[] arr = str.split("\n");

        for(String s : arr)
            list.add(s);
        return list;

    }

}
