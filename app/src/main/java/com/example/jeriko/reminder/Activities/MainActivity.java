package com.example.jeriko.reminder.Activities;

import android.content.ClipData;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.Fragments.GroupListFragment;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Fragments.TaskListFragment;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.ItemTask;
import com.example.jeriko.reminder.Tasks.Note;
import com.example.jeriko.reminder.Tasks.RegularTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;
import com.example.jeriko.reminder.Tasks.TimeTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public final static int SIMPLE_SORT=1;
    public final static int GROUP_SORT=2;
    public final static int TIME_SORT=3;
    public final static int REGULAR_SORT=4;
    public final static int ITEM_SORT=5;
    public final static int NOTE_SORT=6;

    private int sortMethod=SIMPLE_SORT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void addNewTask(View view){
        LinearLayout layout = findViewById(R.id.type_Task_List);

        if(layout.getVisibility()==View.INVISIBLE)
            layout.setVisibility(View.VISIBLE);
        else
            layout.setVisibility(View.INVISIBLE);
    }
    public void createTask(View view){
        LinearLayout layout = findViewById(R.id.type_Task_List);
        switch (view.getId()){
            case R.id.base_task: Intent intent = new Intent(getBaseContext(),TimeTaskActivity.class);
                intent.putExtra("Type", Task.TIME_TASK);
                layout.setVisibility(View.INVISIBLE);
                startActivity(intent);
                break;
            case R.id.regular_task: intent = new Intent(getBaseContext(), RegularTaskActivity.class);
                intent.putExtra("Type",Task.REGULAR_TASK);
                layout.setVisibility(View.INVISIBLE);
                startActivity(intent);
                break;

            case R.id.item_task: intent = new Intent(getBaseContext(),ItemTaskActivity.class);
                intent.putExtra("Type",Task.ITEM_TASK);
                layout.setVisibility(View.INVISIBLE);
                startActivity(intent); break;
            case R.id.note: intent = new Intent(getBaseContext(), NoteActivity.class);
                intent.putExtra("Type",SimpleRecord.NOTE);
                layout.setVisibility(View.INVISIBLE);
                startActivity(intent); break;
        }
    }

    @Override
    public void onStart(){
        super.onStart();

        System.out.println("START 1");
        DataLab.get().makeGetArray();
        DataLab.clearAll();

        System.out.println("START 2");
        JSON.get().readJson(this);
    }
    @Override
    public void onResume(){
        super.onResume();

        System.out.println("RESUME 1");
        switch(sortMethod) {
            case SIMPLE_SORT:
                simpleSortFragment();
                System.out.println("RESUME 2");break;
            case GROUP_SORT:
                groupsSortFragment();break;
            default:
                break;
        }
    }
    @Override
        public void onDestroy() {
        super.onDestroy();

    }
    @Override
        public void onPause(){
            super.onPause();
    }

    public void sortTask(View view){
        PopupMenu menu = new PopupMenu(this,view);
        menu.inflate(R.menu.sort_popup_menu);

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.firstItem:
                            simpleSortFragment();
                            sortMethod=SIMPLE_SORT;
                        break;
                    case R.id.secondItem:
                            groupsSortFragment();
                            sortMethod=GROUP_SORT;
                        break;
                    case R.id.thirdItem:
                        Toast.makeText(getBaseContext(),"THIRD",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.fourthItem:
                        Toast.makeText(getBaseContext(),"FOURTH",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.fifthItem:
                        Toast.makeText(getBaseContext(),"FIFTH",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.sixthItem:
                        Toast.makeText(getBaseContext(),"SIXTH",Toast.LENGTH_LONG).show();
                        break;
                }
            return true;
            }
        });
        menu.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu popupMenu) {
                Toast.makeText(getBaseContext(),"DISMISS",Toast.LENGTH_LONG).show();
            }
        });
        menu.show();
    }
    private void simpleSortFragment(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.container);

        if(fragment!=null)
            fm.beginTransaction().remove(fragment).commit();

        Fragment fragmentNew = new TaskListFragment();
        fm.beginTransaction()
                .add(R.id.container,fragmentNew)
                .commit();

    }
    private void groupsSortFragment(){
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.container);
        if(fragment!=null)
            fm.beginTransaction().remove(fragment).commit();

        Fragment fragmentNew = new GroupListFragment();
        fm.beginTransaction()
                .add(R.id.container,fragmentNew)
                .commit();
    }
}
