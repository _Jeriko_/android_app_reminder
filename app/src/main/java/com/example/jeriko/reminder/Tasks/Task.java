package com.example.jeriko.reminder.Tasks;

import android.widget.Toast;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

public class Task extends SimpleRecord{
    private String title;
    private Date dateCreate;
    private Date dateChange;
    private ArrayList<String> tasksList;
    private int type;
    private String group;

    public final static int TIME_TASK=1;
    public final static int ITEM_TASK=2;
    public final static int REGULAR_TASK=3;


    public Task(String title,Date dateCreate,Date dateChange,ArrayList<String> tasksList,String group,int type){
        super(dateCreate,dateChange,tasksList,group,type);
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
}
