package com.example.jeriko.reminder.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jeriko.reminder.Activities.ShowItemTaskActivity;
import com.example.jeriko.reminder.Activities.ShowNoteActivity;
import com.example.jeriko.reminder.Activities.ShowRegularTaskActivity;
import com.example.jeriko.reminder.Activities.ShowTimeTaskActivity;
import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;

public class TaskListFragment extends Fragment {
    private RecyclerView taskRecyclerView;
    private TaskAdapter adapter;
    private String type;
    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
            View view = inflater.inflate(R.layout.fragment_recycler_list,container,false);

            taskRecyclerView = view.findViewById(R.id.recycler_view);

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

            taskRecyclerView.setLayoutManager(layoutManager);

            updateUI();

            return view;
    }
    private void updateUI(){
        ArrayList<SimpleRecord> list = DataLab.get().makeGetArray();

        adapter = new TaskAdapter(list);
        taskRecyclerView.setAdapter(adapter);
    }
    private class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView title;
        private TextView date;
     //   private TextView tasks;
        private SimpleRecord record;
        //private int type;
        private  LinearLayout layout;
        private int index;

        public TaskHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.list_item_task,parent,false));

            layout = itemView.findViewById(R.id.list_item_layout);
            title = itemView.findViewById(R.id.title_task);
            date = itemView.findViewById(R.id.date_task);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bind(SimpleRecord record){
            this.record = record;

            if(record instanceof Task)
                title.setText(((Task)record).getTitle());

            date.setText(record.getDateCreate().toString());

            switch (record.getType()){
                case Task.TIME_TASK: layout.setBackgroundColor(Color.RED); break;
                case Task.REGULAR_TASK: layout.setBackgroundColor(Color.GREEN); break;
                case Task.ITEM_TASK: layout.setBackgroundColor(Color.BLUE); break;
                case SimpleRecord.NOTE: layout.setBackgroundColor(Color.BLACK); break;
            }
        }
        @Override
        public void onClick(View view){
            switch (record.getType()){
                case Task.TIME_TASK:
                    Intent intent = new Intent(getActivity(), ShowTimeTaskActivity.class);
                        intent.putExtra("Index",taskRecyclerView.getChildLayoutPosition(view));
                        startActivity(intent);
                    break;
                case Task.REGULAR_TASK: Intent intent2 = new Intent(getActivity(), ShowRegularTaskActivity.class);
                        intent2.putExtra("Index",taskRecyclerView.getChildLayoutPosition(view));
                        startActivity(intent2);
                        break;
                case Task.ITEM_TASK: Intent intent3 = new Intent(getActivity(), ShowItemTaskActivity.class);
                        intent3.putExtra("Index",taskRecyclerView.getChildLayoutPosition(view));
                        startActivity(intent3);
                        break;
                case SimpleRecord.NOTE: Intent intent4 = new Intent(getActivity(), ShowNoteActivity.class);
                        intent4.putExtra("Index",taskRecyclerView.getChildLayoutPosition(view));
                        startActivity(intent4);
                        break;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            TaskDeleteDialogFragment dialogFragment = new TaskDeleteDialogFragment();
            FragmentManager fm = getFragmentManager();

            Bundle bundle = new Bundle();
            bundle.putInt("Index",taskRecyclerView.getChildLayoutPosition(v));
            bundle.putInt("Type",record.getType());

            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm,"dialog");
            return true;
        }
    }
    private class TaskAdapter extends RecyclerView.Adapter<TaskHolder>{
        public ArrayList<SimpleRecord> records;

        public TaskAdapter(ArrayList<SimpleRecord> records){
            this.records = records;
        }

        @NonNull
        @Override
        public TaskHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new TaskHolder(layoutInflater,viewGroup);
        }
        @Override
        public void onBindViewHolder(@NonNull TaskHolder taskHolder, int i) {
            taskHolder.bind(records.get(i));
        }
        @Override
        public int getItemCount() {
            return records.size();
        }
    }
}