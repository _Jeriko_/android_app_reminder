package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.ItemTask;

import java.util.ArrayList;
import java.util.Date;

public class ItemTaskActivity extends Activity{
    @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.add_item_task);
    }
    public void addNewItemTask(View view){
        EditText title = findViewById(R.id.title_item_task);
        EditText text = findViewById(R.id.items_task);
        EditText group = findViewById(R.id.group_item_task);

        ArrayList<String> list = new ArrayList<>();
        String[]arr = text.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        ItemTask newTask = new ItemTask(title.getText().toString(),new Date(),new Date(),list,group.getText().toString());
        DataItemTasks.get().add(newTask);

        DataGroups.get().add(new GroupRecord(newTask));

        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelItemTask(View view){
        finish();
    }
}