package com.example.jeriko.reminder.Tasks;

import java.util.ArrayList;
import java.util.Date;

public class ItemTask extends Task {
    private String title;
    private Date dateCreate;
    private Date dateChange;
    private ArrayList<String> tasksList;
    private int type;
    private String group;

    public ItemTask(String title,Date dateCreate,Date dateChange,ArrayList<String> tasksList, String group){
        super(title,dateCreate,dateChange,tasksList,group,ITEM_TASK);
    }
}
