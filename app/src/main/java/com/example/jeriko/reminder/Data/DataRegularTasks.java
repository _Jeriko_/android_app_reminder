package com.example.jeriko.reminder.Data;

import com.example.jeriko.reminder.Tasks.RegularTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;

public class DataRegularTasks{
    private ArrayList<SimpleRecord> listTasks;
    private static DataRegularTasks regularTasks;

    private DataRegularTasks(){
        listTasks = new ArrayList<>();
    }
    public static DataRegularTasks get(){
        if(regularTasks==null)
            regularTasks = new DataRegularTasks();
        return regularTasks;
    }
    public void add(SimpleRecord record){
        listTasks.add(record);
    }
    public ArrayList<SimpleRecord> getListRegularTasks(){
        return listTasks;
    }
}
