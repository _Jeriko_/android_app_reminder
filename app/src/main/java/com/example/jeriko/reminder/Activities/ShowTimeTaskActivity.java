package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.ItemTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;
import com.example.jeriko.reminder.Tasks.TimeTask;

import java.util.ArrayList;
import java.util.Date;

public class ShowTimeTaskActivity extends Activity {
    private EditText titleEditText;
    private EditText taskEditText;
    private EditText groupEditText;
    private TextView dateTextView;
    private SimpleRecord record;
    @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.show_time_task);

        int index = getIntent().getIntExtra("Index",0);

        record = DataLab.get().makeGetArray().get(index);

        titleEditText = findViewById(R.id.show_time_task_title);
        taskEditText = findViewById(R.id.show_time_tasks);
        groupEditText = findViewById(R.id.show_time_task_group);
        dateTextView = findViewById(R.id.show_time_task_date);

        titleEditText.setText(((Task)record).getTitle());
        taskEditText.setText(record.listToString());
        groupEditText.setText(record.getGroup());
        dateTextView.setText(record.getDateCreate().toString());
    }
    public void changeTask(View view){
        int index = DataTimeTasks.get().getListTimeTasks().indexOf(record);

        ArrayList<String> list = new ArrayList<>();
        String[]arr = taskEditText.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        DataTimeTasks.get().getListTimeTasks().set(index, new TimeTask(titleEditText.getText().toString(), record.getDateCreate(), new Date(),list,groupEditText.getText().toString()));
        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelShowTasks(View view){
        finish();
    }
}
