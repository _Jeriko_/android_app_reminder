package com.example.jeriko.reminder.Tasks;

import android.support.annotation.ArrayRes;

import com.example.jeriko.reminder.Tasks.SimpleRecord;

import java.util.ArrayList;
import java.util.Date;

public class GroupRecord {
    private String name;
    private Date dateCreate;
    private ArrayList<SimpleRecord> list = new ArrayList<>();

    public GroupRecord(SimpleRecord record){
        this.name = record.getGroup();
        this.dateCreate = new Date();
        this.list.add(record);
    }
    public String getName(){
        return this.name;
    }
    public Date getDateCreate(){
        return this.dateCreate;
    }

    public ArrayList<SimpleRecord> getList() {
        return list;
    }
}
