package com.example.jeriko.reminder.Data;

import com.example.jeriko.reminder.Tasks.SimpleRecord;

import java.util.ArrayList;

public class DataNotes {
    private ArrayList<SimpleRecord> listNote;
    private static DataNotes dataNote;

    private DataNotes(){
        listNote = new ArrayList<>();
    }
    public static DataNotes get(){
        if(dataNote==null)
            dataNote = new DataNotes();
        return dataNote;
    }
    public void add(SimpleRecord record){
        listNote.add(record);
    }
    public ArrayList<SimpleRecord> getListNote() {
        return listNote;
    }
}
