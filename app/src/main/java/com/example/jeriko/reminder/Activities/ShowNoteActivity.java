package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.Note;
import com.example.jeriko.reminder.Tasks.RegularTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;
import java.util.Date;

public class ShowNoteActivity extends Activity {
    private SimpleRecord record;
    private EditText taskEditText;
    private EditText groupEditText;
    private TextView dateTextView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_note);

        int index = getIntent().getIntExtra("Index",0);

        record = DataLab.get().makeGetArray().get(index);

        taskEditText = findViewById(R.id.show_notes);
        groupEditText = findViewById(R.id.show_note_group);
        dateTextView = findViewById(R.id.show_note_date);

        taskEditText.setText(record.listToString());
        groupEditText.setText(record.getGroup());
        dateTextView.setText(record.getDateCreate().toString());
    }
    public void changeTask(View view){
        int index = DataNotes.get().getListNote().indexOf(record);

        ArrayList<String> list = new ArrayList<>();
        String[]arr = taskEditText.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        DataNotes.get().getListNote().set(index, new Note(record.getDateCreate(), new Date(),list,groupEditText.getText().toString()));
        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelShowNote(View view){
        finish();
    }
}
