package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataLab;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.ItemTask;
import com.example.jeriko.reminder.Tasks.RegularTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;
import java.util.Date;

public class ShowItemTaskActivity extends Activity {
    private SimpleRecord record;
    private EditText titleEditText;
    private EditText taskEditText;
    private EditText groupEditText;
    private TextView dateTextView;
    @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.show_item_task);

            int index = getIntent().getIntExtra("Index",0);

        record = DataLab.get().makeGetArray().get(index);

        titleEditText = findViewById(R.id.show_item_task_title);
        taskEditText = findViewById(R.id.show_item_tasks);
        groupEditText = findViewById(R.id.show_item_task_group);
        dateTextView = findViewById(R.id.show_item_task_date);


        titleEditText.setText(((Task)record).getTitle());
        taskEditText.setText(record.listToString());
        groupEditText.setText(record.getGroup());
        dateTextView.setText(record.getDateCreate().toString());
    }
    public void changeTask(View view){
        int index = DataItemTasks.get().getListItemTasks().indexOf(record);

        ArrayList<String> list = new ArrayList<>();
        String[]arr = taskEditText.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        DataItemTasks.get().getListItemTasks().set(index, new ItemTask(titleEditText.getText().toString(), record.getDateCreate(), new Date(),list,groupEditText.getText().toString()));
        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelShowTasks(View view){
        finish();
    }
}
