package com.example.jeriko.reminder.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataItemTasks;
import com.example.jeriko.reminder.Data.DataNotes;
import com.example.jeriko.reminder.Data.DataRegularTasks;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class RecordGroupListFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecordGroupAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_list,container,false);

        recyclerView = view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        updateUI();
        return view;
    }
    private void updateUI(){
            String name =  getArguments().getString("Name");
            GroupRecord clickGroup=null;
            for(GroupRecord gr : DataGroups.get().getGroupRecordList()){
                if(name.compareTo(gr.getName())==0)
                {
                    clickGroup=gr;
                    break;
                }
            }

            adapter = new RecordGroupAdapter(clickGroup.getList());

        recyclerView.setAdapter(adapter);
    }
    private class RecordGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        private TextView title;
        private TextView dateCreate;
        private SimpleRecord record;
        private LinearLayout layout;

        public RecordGroupHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.list_item_task,parent,false));

            layout = itemView.findViewById(R.id.list_item_layout);
            title = itemView.findViewById(R.id.title_task);
            dateCreate = itemView.findViewById(R.id.date_task);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }
        public void bind(SimpleRecord record){
            this.record = record;

            if(record.getType()!=SimpleRecord.NOTE)
                title.setText(((Task)record).getTitle());

            dateCreate.setText(record.getDateCreate().toString());

            switch (record.getType()){
                case Task.TIME_TASK: layout.setBackgroundColor(Color.RED); break;
                case Task.REGULAR_TASK: layout.setBackgroundColor(Color.GREEN); break;
                case Task.ITEM_TASK: layout.setBackgroundColor(Color.BLUE); break;
                case SimpleRecord.NOTE: layout.setBackgroundColor(Color.BLACK); break;
            }
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(),"CLICK",Toast.LENGTH_LONG).show();
        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(getContext(),"LONG CLICK",Toast.LENGTH_LONG).show();
            return false;
        }
    }
    private class RecordGroupAdapter extends RecyclerView.Adapter<RecordGroupHolder>{
        private ArrayList<SimpleRecord> list;
        public RecordGroupAdapter(ArrayList<SimpleRecord> list){
            this.list = list;
        }
        @NonNull
        @Override
        public RecordGroupHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new RecordGroupHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull RecordGroupHolder recordGroupHolder, int i) {
            recordGroupHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
