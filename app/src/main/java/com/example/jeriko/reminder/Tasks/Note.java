package com.example.jeriko.reminder.Tasks;

import java.util.ArrayList;
import java.util.Date;

public class Note extends SimpleRecord {
    private Date dateCreate;
    private Date dateChange;
    private ArrayList<String> taskList;
    private String group;
    private int type;

    public Note(Date dateCreate,Date dateChange,ArrayList<String> taskList,String group){
        super(dateCreate,dateChange,taskList,group,Task.NOTE);
    }
}
