package com.example.jeriko.reminder.Data;

import com.example.jeriko.reminder.Tasks.ItemTask;
import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;

import java.util.ArrayList;
import java.util.Date;

public class DataItemTasks{
    private ArrayList<SimpleRecord> listTasks;
    private static DataItemTasks itemTasks;

    private DataItemTasks(){
        listTasks = new ArrayList<>();
    }
    public static DataItemTasks get(){
        if(itemTasks==null)
            itemTasks = new DataItemTasks();
        return itemTasks;
    }
    public void add(SimpleRecord record){
        listTasks.add(record);
    }
    public ArrayList<SimpleRecord> getListItemTasks(){
        return listTasks;
    }
}
