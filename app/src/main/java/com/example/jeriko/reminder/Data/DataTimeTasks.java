package com.example.jeriko.reminder.Data;

import com.example.jeriko.reminder.Tasks.SimpleRecord;
import com.example.jeriko.reminder.Tasks.Task;
import com.example.jeriko.reminder.Tasks.TimeTask;

import java.util.ArrayList;

public class DataTimeTasks {
    private ArrayList<SimpleRecord> listTasks;
    private static DataTimeTasks dtt;

    private DataTimeTasks(){
        listTasks = new ArrayList<>();
    }
    public static DataTimeTasks get(){
        if(dtt==null)
            dtt = new DataTimeTasks();
        return dtt;
    }
    public void add(SimpleRecord record){
        listTasks.add(record);
    }
    public ArrayList<SimpleRecord> getListTimeTasks(){
        return listTasks;
    }
}
