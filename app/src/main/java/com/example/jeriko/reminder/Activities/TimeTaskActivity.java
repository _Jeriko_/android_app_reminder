package com.example.jeriko.reminder.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jeriko.reminder.Data.DataGroups;
import com.example.jeriko.reminder.Data.DataTimeTasks;
import com.example.jeriko.reminder.JSON.JSON;
import com.example.jeriko.reminder.R;
import com.example.jeriko.reminder.Tasks.GroupRecord;
import com.example.jeriko.reminder.Tasks.TimeTask;

import java.util.ArrayList;
import java.util.Date;

public class TimeTaskActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_time_task);
    }
    public void addNewTimeTask(View view){
        EditText title = findViewById(R.id.title_task);
        EditText text = findViewById(R.id.timeTasks);
        EditText group = findViewById(R.id.group_time_task);

        ArrayList<String> list = new ArrayList<>();

        String[]arr = text.getText().toString().split("\n");

        for(String s : arr)
            list.add(s);

        TimeTask newTask = new TimeTask(title.getText().toString(),new Date(),new Date(),list,group.getText().toString());
        DataTimeTasks.get().add(newTask);

        DataGroups.get().add(new GroupRecord(newTask));

        JSON.get().saveAndWrite(this);
        finish();
    }
    public void cancelTimeTask(View view){
       finish();
    }
}
